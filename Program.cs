﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Servicios;
using Util;
using System.Data;

namespace ConsoleAppObtenerDivisa
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Divisa divisa = new Divisa();
                double PrecioDolar = 0;
                PrecioDolar = divisa.ObtenerPrecioDolar();

                if(PrecioDolar > 0)
                {
                    Console.Write("Fecha de Cotización: "+ DateTime.Today.ToShortDateString());
                    Console.Write("\n" + "\n" + "Precio del dolar: $" + PrecioDolar);
                    Console.ReadKey();

                }
            }
            catch (Exception ex)
            {
                LogUtilities.Log("ConsoleAppObtenerDivisa --> Program --> Main --> \" " + ex.Message + " \"");
            }           

        }
    }
}
