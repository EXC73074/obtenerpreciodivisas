﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public class LogUtilities
    {
        public static void Log(String pLogMessage)
        {
            try
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["CreateLog"]))
                {
                    string folderApp = Path.GetDirectoryName(ConfigurationManager.AppSettings["PathLog"]);

                    using (StreamWriter debugFile =
                        File.AppendText(Path.Combine(folderApp, "AsambleaDigital_Log.txt")))
                    {

                        debugFile.Write("\r\nEvento : ");
                        debugFile.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                        debugFile.WriteLine("  >>{0}", pLogMessage);
                        debugFile.WriteLine("-------------------------------");
                        debugFile.Flush();
                        debugFile.Close();
                    };
                }
            }
            catch (Exception ex)
            {
                //TODO: manejar esto de alguna forma.
            }
        }
    }
}
