﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Configuration;
using Util;


namespace Servicios
{
    public class Divisa
    {
        HtmlDocument Doc = new HtmlDocument();
        HtmlWeb Web = new HtmlWeb();
        private static string UrlBanco = ConfigurationManager.AppSettings["UrlBanco"];

        public double ObtenerPrecioDolar()
        {
            try
            {
                double dolar = 0;
                string mensaje = string.Empty;
                Web.UserAgent = @"Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)";
                Doc = Web.Load(UrlBanco);

                //obtenemos div
                HtmlNode Div_Billetes = Doc.DocumentNode.SelectSingleNode("//div[@id='billetes']");

                if(Div_Billetes != null)
                {
                    if (!Div_Billetes.HasChildNodes)
                    {
                        LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerPrecioDolar --> \" El nodo Div_Billetes es vacio \"");
                        return 0;
                    }
                    
                }
                else
                {
                    LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerPrecioDolar --> \" No fue posible obtener el nodo Div_Billetes \"");
                    return 0;
                }

                HtmlNodeCollection Nodes = Div_Billetes.ChildNodes;
                HtmlNode Tabla_Cotizacion = null;
                foreach(HtmlNode node in Nodes)
                {
                    if(node.HasClass("table"))
                    {
                        if (node.HasClass("cotizacion"))
                        {
                            Tabla_Cotizacion = node;
                        }
                    }
                }

                if (Tabla_Cotizacion != null)
                {
                    if (!Tabla_Cotizacion.HasChildNodes)
                    {
                        LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerPrecioDolar --> \" El nodo Tabla_Cotizacion es vacio \"");
                        return 0;
                    }

                }
                else
                {
                    LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerPrecioDolar --> \" No fue posible obtener el nodo Tabla_Cotizacion \"");
                    return 0;
                }

                DateTime Fecha_Cotizacion = ObtenerFechaCotizacion(Tabla_Cotizacion,ref mensaje);

                if (Fecha_Cotizacion.Year == 0001)
                {
                    LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerFechaCotizacion --> \" "+mensaje+" \"");
                    return 0;
                }
                else
                {
                    if(Fecha_Cotizacion == DateTime.Today)
                    {
                        dolar = ObtenerCotizacionDolar(Tabla_Cotizacion, ref mensaje);

                        if (dolar == 0)
                        {
                            LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerCotizacionDolar --> \" " + mensaje + " \"");
                            return 0;
                        }
                        else
                        {
                            return dolar;
                        }
                    }
                    else
                    {
                        LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerCotizacionDolar --> \" La fecha de cotización no es igual a la fecha actual \"");
                        return 0;                    
                    }

                } 
            }
            catch (Exception ex)
            {
                LogUtilities.Log("ConsoleAppObtenerDivisa --> Servicios --> Divisa --> ObtenerCotizacionDolar --> \" "+ex.Message+" \"");
                return 0; 
            }              
            
        }

        private DateTime ObtenerFechaCotizacion(HtmlNode Tabla_Cotizacion, ref string mensaje)
        {
            try
            {
                DateTime fecha = DateTime.MinValue; //  01-01-0001

                //obtenemos coleccion de th en tabla --> Head --> tr
                HtmlNodeCollection Th = Tabla_Cotizacion.Element("thead").Element("tr").ChildNodes;

                if (Th != null)
                {
                    if (Th.Count == 0)
                    {
                        mensaje = "El Nodo \"Th\" esta vacio";
                        return fecha;
                    }
                }
                else
                {
                    mensaje = "No se encontró el Nodo \"Th\"";
                    return fecha;
                }

                foreach (HtmlNode nodo in Th)
                {
                    if(nodo.HasClass("fechaCot"))
                    {
                        fecha = Convert.ToDateTime(nodo.InnerHtml);
                    }
                }

                return fecha;
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                DateTime fecha = DateTime.MinValue; 
                return fecha;
            }
        }

        private double ObtenerCotizacionDolar(HtmlNode Tabla_Cotizacion, ref string mensaje)
        {
            try
            {
                double dolar = 0;

                // obtiene una lista de nodos TD dentro de Tabla --> tbody --> primer Tr
                HtmlNodeCollection Td = Tabla_Cotizacion.Element("tbody").Element("tr").ChildNodes;

                if(Td.Count > 0)
                {
                    dolar = Convert.ToDouble(Td[5].InnerHtml);
                }
                else
                {
                    mensaje = "El nodo \"Td\" es vacio";
                    return 0;
                }
                return dolar;
               
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                return 0;
            }
        }

    }
}
